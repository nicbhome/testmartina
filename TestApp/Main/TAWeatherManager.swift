//
//  TAWeatherManager.swift
//  TestApp
//
//  Created by Nicolay on 23/01/2019.
//  Copyright © 2019 Nicolay. All rights reserved.
//

import UIKit
import CoreLocation

class TAWeatherManager: NSObject
{
    //var _jsonSerialized = nil
    
    func findFor(location: CLLocation)
    {
        DispatchQueue.global(qos: .userInitiated).async {
            
            //let image = self.loadOrGenerateAnImage()
            // Bounce back to the main thread to update the UI
            self.updateWeather(location: location)
            
            DispatchQueue.main.async {
                //self.imageView.image = image
            }
        }
    }
    
    func updateWeather(location: CLLocation)
    {
        /*
        var response: URLResponse?
        var error: NSError?
        let urlData = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
    
        if let httpResponse = response as? NSHTTPURLResponse {
            println(httpResponse.statusCode)
        }
         */
        let s       = String.init(format:"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%@", location.coordinate.latitude, location.coordinate.longitude, "f8ebe83b271f82737f48d532d6967760")
        
        let request = URLRequest(url: NSURL(string: s)! as URL)
        
            // Perform the request
        var response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request, returning: response)
            
                // Convert the data to JSON
            let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fireUpdateWeather"), object: jsonSerialized )
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
    }
}
