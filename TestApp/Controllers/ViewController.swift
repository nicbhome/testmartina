//
//  ViewController.swift
//  TestApp
//
//  Created by Nicolay on 23/01/2019.
//  Copyright © 2019 Nicolay. All rights reserved.
//

import UIKit

class ViewController: BaseViewController
{
    @IBOutlet weak var _todayBt         = nil as TButton?
    @IBOutlet weak var _forecastBt      = nil as TButton?
    
    @IBOutlet weak var _todayView       = nil as UIView?
    @IBOutlet weak var _forecastView    = nil as UIView?
    
    @IBOutlet weak var _titleView       = nil as UINavigationItem?
    
    //TALocationManager
    let _locationManager                = TALocationManager()
    let _weatherManager                 = TAWeatherManager()
    var _selectedTab                    = 0 as Int
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.configControls()
        
        _locationManager.start()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(fireUpdateLocation),
            name: NSNotification.Name(rawValue: "fireUpdateLocation"),
            object: nil)
    }
    
    func configControls()
    {
        if(_selectedTab == 0)
        {
            _todayBt?.selectedButton    = true
            _forecastBt?.selectedButton = false
            
            _todayView?.isHidden        = false
            _forecastView?.isHidden     = true
        }
        else
        {
            _todayBt?.selectedButton    = false
            _forecastBt?.selectedButton = true
            
            _todayView?.isHidden        = true
            _forecastView?.isHidden     = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)
        
        if  segue.destination is TodayViewController
        //if(segue.identifier == "today")
        {
            // obj is a string array. Do something with stringArray
            let item = segue.destination as! TodayViewController
            item.setup()
        }
        
        if  segue.destination is ForecastViewController
        {
            // obj is a string array. Do something with stringArray
            let item = segue.destination as! ForecastViewController
            item.setup()
        }
    }
    
    @objc func fireUpdateLocation()
    {
        DispatchQueue.main.async
        {
            self._titleView?.title = self._locationManager.city
            
            self._weatherManager.findFor(location: self._locationManager.location!)
        }
    }
    
    //MARK - Private
    
    @IBAction func onSelect(_ sender: UIButton)
    {
        let tag         = sender.tag
        _selectedTab    = tag - 1
        
        self.configControls()
    }
}

