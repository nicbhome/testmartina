//
//  TodayViewController.swift
//  TestApp
//
//  Created by Nicolay on 23/01/2019.
//  Copyright © 2019 Nicolay. All rights reserved.
//

import UIKit

class TodayViewController: UIViewController {

    
    func setup()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(fireUpdateLocation),
            name: NSNotification.Name(rawValue: "fireUpdateLocation"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(fireUpdateWeather),
            name: NSNotification.Name(rawValue: "fireUpdateWeather"),
            object: nil)

        
        hideControls(hide:true)
    }
    
    func hideControls(hide:Bool)
    {
        let v = self.view.viewWithTag(12) as! UIView
        let v1 = self.view.viewWithTag(1) as! UIView
        let v2 = self.view.viewWithTag(2) as! UIView
        let v3 = self.view.viewWithTag(10) as! UIView
        let v4 = self.view.viewWithTag(11) as! UIView
        
        v.isHidden = hide
        v1.isHidden = hide
        v2.isHidden = hide
        v3.isHidden = hide
        v4.isHidden = hide
        
        let ac = self.view.viewWithTag(20) as! UIActivityIndicatorView
        ac.isHidden = !hide
        
    }
    
    @objc func fireUpdateLocation(object:NSNotification)
    {
        let locationManager = object.object as! TALocationManager
        
        DispatchQueue.main.async
        {
            let l = self.view.viewWithTag(1) as! UILabel
            l.text = String.init(format:"%@, %@", locationManager.city!, locationManager.country! )
        }
    }
    
    @objc func fireUpdateWeather(object:NSNotification)
    {
        let jsonSerialized = object.object as? [String : Any]
        
        DispatchQueue.main.async
        {
            if(jsonSerialized != nil)
            {
                //print(jsonSerialized)
                
                let l = self.view.viewWithTag(2) as! UILabel
                
                let main    = jsonSerialized!["main"] as! [String : Any]
                let weather = jsonSerialized!["weather"] as! [[String : Any]]
                
                var temp    = main["temp"] as! Double//int n                       = ([main[@"temp"] intValue] - 273.15);
                temp        = temp - 273.15
                let s       = weather[0]["main"] as! String
                
                l.text = String.init(format:"%.02f˚C | %@", temp, s  );
                
                self.hideControls(hide:false)
            }
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
