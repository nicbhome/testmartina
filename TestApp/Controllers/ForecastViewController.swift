//
//  ForecastViewController.swift
//  TestApp
//
//  Created by Nicolay on 23/01/2019.
//  Copyright © 2019 Nicolay. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tableView  = nil as UITableView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 8
    }
    
    func numberOfSections(in tableView: UITableView)  -> Int
    {
        return 5
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int)  -> String
    {
        if(section == 0)
        {
            return "Today"
        }
        else
        {
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "EEEE"
            let myDate                  = Date()
            let tomorrow                = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
            let somedateString          = dateFormatter.string(from: tomorrow!)
            
            return somedateString
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell    = tableView.dequeueReusableCell(withIdentifier: "day", for: indexPath)
        let label   = cell.viewWithTag(1) as! UILabel
        
        label.text  = String.init(format:"%02d:00", indexPath.row * 3)
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setup()
    {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(fireUpdateLocation),
            name: NSNotification.Name(rawValue: "fireUpdateLocation"),
            object: nil)
    }
    
    @objc func fireUpdateLocation(object:NSNotification)
    {
        let locationManager = object.object as! TALocationManager
        
        DispatchQueue.main.async
            {
                let l = self.view.viewWithTag(1) as! UILabel
                l.text = String.init(format:"%@, %@", locationManager.city!, locationManager.country! )
        }
    }

}
