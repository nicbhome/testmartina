//
//  TALocationManager.swift
//  TestApp
//
//  Created by Nicolay on 23/01/2019.
//  Copyright © 2019 Nicolay. All rights reserved.
//

import UIKit
import CoreLocation

class TALocationManager: NSObject, CLLocationManagerDelegate {

    let locationManager = CLLocationManager()
    
    var location        = nil as CLLocation?
    var city            = nil as String?
    var country         = nil as String?
    
    func start()
    {
        let authState                                           = CLLocationManager.authorizationStatus()
        
        locationManager.delegate                                = self
        
        if( authState == CLAuthorizationStatus.authorizedWhenInUse )
        {
            locationManager.startUpdatingLocation()
        }
        else
        {
            requestAuthorization();
        }
    }
    
    
    @objc func requestAuthorization()
    {
        //print("requestAlwaysAuthorization()")
        
        let authState = CLLocationManager.authorizationStatus()
        
        if  authState == CLAuthorizationStatus.notDetermined
        {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //MARK: CLLocationManagerDelegate
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if locations.count > 0
        {
            let location        = locations[0] as CLLocation
            
            self.location       = location
            
            let ceo: CLGeocoder = CLGeocoder()
            
            ceo.reverseGeocodeLocation(location, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    else
                    {
                        let pm = placemarks! as [CLPlacemark]
                        
                        if pm.count > 0
                        {
                            let pm          = placemarks![0]
                            self.city       = pm.locality
                            self.country    = pm.country
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fireUpdateLocation"), object: self )
                        }
                    }
            })
        }
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        let authState = CLLocationManager.authorizationStatus()
        
        if( authState == CLAuthorizationStatus.authorizedWhenInUse )
        {
            locationManager.startUpdatingLocation()
        }
    }
}
