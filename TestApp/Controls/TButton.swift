//
//  TButton.swift
//  TestApp
//
//  Created by Nicolay on 23/01/2019.
//  Copyright © 2019 Nicolay. All rights reserved.
//

import UIKit

class TButton: UIButton {

   
    var  _selected  = false as Bool
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        
        self.adjustImageAndTitleOffsetsForButton ()
    }
    
    
    public func adjustImageAndTitleOffsetsForButton () {
        
        let spacing: CGFloat = 0.0
        
        let imageSize = self.imageView!.frame.size
        
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0)
        
        let titleSize = self.titleLabel!.frame.size
        
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0, bottom: 0, right: -titleSize.width)
    }

    public var selectedButton: Bool {
        set {
        
            if(newValue)
            {
                self.tintColor = self.superview?.tintColor
            }
            else
            {
                self.tintColor = UIColor.gray
            }
            
            _selected = true
        }
        get
        {
            return _selected;
        }
    }
    
}
